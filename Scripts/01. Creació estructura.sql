 -- Creació de l'estructura de la base de dades i taules

 create or replace database beatdown;

 -- Taules

CREATE OR REPLACE TABLE beatdown.notification (
	notification_id INT NOT NULL auto_increment primary key,
	creation_date DATETIME NOT NULL,
	title_cat varchar(150) NOT NULL,
	title_esp varchar(150) NULL,
	title_eng varchar(150) NULL,
	user_id INT NOT NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE OR REPLACE TABLE beatdown.user (
	user_id INT NOT NULL auto_increment primary key,
	creation_date DATETIME NOT NULL,
	modification_date DATETIME null,
	email varchar(200) NOT NULL,
	password varchar(150) NULL,
	avatar blob null
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE OR REPLACE TABLE beatdown.personal_offer (
	personal_offer_id INT NOT NULL auto_increment primary key,
	creation_date DATETIME NOT NULL,
	user_id INT not null,
	stablishment_id INT NOT NULL,
	offer_id INT NOT NULL,
	code varchar(33) not null,
	expiration_date DATETIME not null
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE OR REPLACE TABLE beatdown.stablishment (
	stablishment_id INT NOT NULL auto_increment primary key,
	name varchar(100) not null,
	address varchar(200) not null,
	city varchar(80) not null,
	latitude double NOT NULL,
	longitude double NOT NULL,
	stablishment_category_id int(33) not null,
	location POINT NULL DEFAULT NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE OR REPLACE TABLE beatdown.stablishment_category (
	stablishment_category_id INT NOT NULL auto_increment primary key,
	description_cat varchar(200) not null,
	description_spa varchar(200) not null,
	description_eng varchar(200) not null
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE OR REPLACE TABLE beatdown.favorite_offer (
	favorite_offer_id INT NOT NULL AUTO_INCREMENT primary key,
	offer_id INT NOT null,
	user_id INT NOT null
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE OR REPLACE TABLE beatdown.offer (
	offer_id INT NOT NULL auto_increment primary key,
	title_cat varchar(150) not null,
	title_spa varchar(150) not null,
	title_eng varchar(150) not null,
	image_url varchar(200) not null,
	discount int not null,
	offer_type_id int not null,
	stablishment_id int not null
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE OR REPLACE TABLE beatdown.offer_type (
	offer_type_id INT NOT NULL auto_increment primary key,
	description_cat varchar(100) not null,
	description_spa varchar(100) not null,
	description_eng varchar(100) not null
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

-- FK

ALTER TABLE beatdown.notification ADD CONSTRAINT notification_user_fk FOREIGN KEY (user_id) REFERENCES beatdown.`user`(user_id);
ALTER TABLE beatdown.personal_offer ADD CONSTRAINT personal_offer_stablishment_fk FOREIGN KEY (stablishment_id) REFERENCES beatdown.stablishment(stablishment_id);
ALTER TABLE beatdown.personal_offer ADD CONSTRAINT personal_offer_user_fk FOREIGN KEY (user_id) REFERENCES beatdown.`user`(user_id);
ALTER TABLE beatdown.personal_offer ADD CONSTRAINT personal_offer_offer_fk FOREIGN KEY (offer_id) REFERENCES beatdown.offer(offer_id);
ALTER TABLE beatdown.stablishment ADD CONSTRAINT stablishment_stablishment_category_fk FOREIGN KEY (stablishment_category_id) REFERENCES beatdown.stablishment_category(stablishment_category_id);
ALTER TABLE beatdown.favorite_offer ADD CONSTRAINT favorite_offer_offer_fk FOREIGN KEY (offer_id) REFERENCES beatdown.offer(offer_id);
ALTER TABLE beatdown.favorite_offer ADD CONSTRAINT favorite_offer_user_fk FOREIGN KEY (user_id) REFERENCES beatdown.`user`(user_id);
ALTER TABLE beatdown.offer ADD CONSTRAINT offer_offer_type_fk FOREIGN KEY (offer_type_id) REFERENCES beatdown.offer_type(offer_type_id);
ALTER TABLE beatdown.offer ADD CONSTRAINT offer_stablishment_fk FOREIGN KEY (stablishment_id) REFERENCES beatdown.stablishment(stablishment_id);

-- Noves columnes
ALTER TABLE beatdown.offer ADD code varchar(33) NULL;
ALTER TABLE beatdown.offer ADD CONSTRAINT offer_un UNIQUE KEY (code);
ALTER TABLE beatdown.personal_offer ADD used BOOL DEFAULT 0 NOT NULL;
ALTER TABLE beatdown.user MODIFY COLUMN avatar LONGBLOB DEFAULT NULL NULL;